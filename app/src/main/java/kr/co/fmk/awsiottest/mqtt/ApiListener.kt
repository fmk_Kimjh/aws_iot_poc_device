package kr.co.fmk.awsiottest.mqtt

interface ApiListener {
    fun onSuccess()
    fun onFail(errMsg: String?)
}