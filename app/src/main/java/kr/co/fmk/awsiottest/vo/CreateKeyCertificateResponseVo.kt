package kr.co.fmk.awsiottest.vo

class CreateKeyCertificateResponseVo {
    var certificateId: String? = null
    var certificatePem: String? = null
    var privateKey: String? = null
    var certificateOwnershipToken: String? = null

    override fun toString(): String {
        return """
            CreateKeyCertificateResponseVo(
            certificateId=$certificateId
            certificatePem=$certificatePem
            privateKey=$privateKey
            certificateOwnershipToken=$certificateOwnershipToken
            )
            """
    }
}