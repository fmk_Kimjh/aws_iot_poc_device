package kr.co.fmk.awsiottest.vo

class ErrorResponseVo {
    var errorCode: String? = null
    var errorMessage: String? = null
    var statusCode: String? = null
    override fun toString(): String {
        return """
            ErrorResponseVo(
                    errorCode=$errorCode
                    errorMessage=$errorMessage
                    statusCode=$statusCode
                    )
            """
    }
}