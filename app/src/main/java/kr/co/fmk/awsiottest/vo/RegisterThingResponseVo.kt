package kr.co.fmk.awsiottest.vo

class RegisterThingResponseVo {
    var thingName: String? = null
    var deviceConfiguration: HashMap<String, String>? = null

    override fun toString(): String {
        return """
            RegisterThingResponseVo(
            thingName=$thingName
            deviceConfiguration=${deviceConfiguration.toString()}
            )
            """
    }
}