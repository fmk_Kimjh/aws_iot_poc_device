package kr.co.fmk.awsiottest.mqtt

import android.content.Context
import android.util.Log
import com.amazonaws.mobileconnectors.iot.*
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import kr.co.fmk.awsiottest.Common
import kr.co.fmk.awsiottest.util.JLog
import kr.co.fmk.awsiottest.util.SharedPreferenceManager
import kr.co.fmk.awsiottest.util.Util
import kr.co.fmk.awsiottest.viewmodels.MqttViewModel
import java.security.KeyStore

class MqttConnection(private val ctx: Context) {
    private val TAG = javaClass.simpleName.toString()

    fun connect(thingName: String?, keystore: KeyStore?, viewModel: MqttViewModel?,
                apiListener: ApiListener?) {
        val mqttManager = MqttInstance.getMqttInstance(thingName,
            Region.getRegion(Regions.AP_NORTHEAST_2),
            Common.ACCOUNT_ENDPOINT_PREFIX)

        mqttManager?.connect(keystore,
            AWSIotMqttClientStatusCallback { status, throwable ->
                Log.d(TAG, "status : $status, throwable : ${throwable?.message}")
                if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connected) {
                    JLog.d(TAG, "connect","" + status + ": "+viewModel?.mKeystoreName)
                    if (!SharedPreferenceManager.getPreference(
                            ctx, SharedPreferenceManager.IS_SUCCESSED_PRIVISIONING, false)) {
                        SharedPreferenceManager.setPreference(
                            ctx, SharedPreferenceManager.IS_SUCCESSED_PRIVISIONING, true)
                        SharedPreferenceManager.setPreference(
                            ctx, SharedPreferenceManager.CERT_ID, viewModel?.mSavedCertId)
                        SharedPreferenceManager.setPreference(
                            ctx, SharedPreferenceManager.KEYSTORE_NAME, viewModel?.mKeystoreName)
                        SharedPreferenceManager.setPreference(
                            ctx, SharedPreferenceManager.KEYSTORE_PW, viewModel?.mKeyStorePw)
                    }

                    subscribesShadowTopic(mqttManager, viewModel, thingName)

                    apiListener?.onSuccess()
                } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connecting) {
                    Log.d(TAG,"Connecting...")
                } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Reconnecting) {
                    if (throwable != null) {
                        Log.d(TAG,"Connection error: " + throwable.message)
                        apiListener?.onFail(throwable.message)
                    } else {
                        Log.d(TAG, "Reconnecting")
                    }
                } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.ConnectionLost) {
                    if (throwable != null) {
                        Log.d(TAG,"Connection error.")
                        throwable.printStackTrace()
                    }
                    Log.d(TAG,"Disconnected")
                    apiListener?.onFail(throwable.message)
                } else {
                    Log.d(TAG,"Disconnected")
                    apiListener?.onFail(throwable?.message)
                }
                viewModel?.setPubResult("status : $status, throwable : ${throwable?.message}")
            })
    }

    fun subsribe2Topic(thingName: String?, topic: String?,
                       viewModel: MqttViewModel?, apiListener: ApiListener?) {
        val mqttManager = MqttInstance.getMqttInstance(thingName,
            Region.getRegion(Regions.AP_NORTHEAST_2),
            Common.ACCOUNT_ENDPOINT_PREFIX)

        mqttManager?.subscribeToTopic(topic, AWSIotMqttQos.QOS0,
            object: AWSIotMqttSubscriptionStatusCallback {
                override fun onSuccess() {
                    apiListener?.onSuccess()
                }
                override fun onFailure(exception: Throwable) {
                    apiListener?.onFail(exception?.message)
                }
            },
            object: AWSIotMqttNewMessageCallback {
                override fun onMessageArrived(topic: String?, data: ByteArray?) {
                    viewModel?.setResponseResult("topic : $topic, data : ${data?.let { String(it) }}")

                }
            })

    }

    private fun subscrb2Topic(thingName: String?, topic: String?, viewModel: MqttViewModel?) {
        subsribe2Topic(thingName, topic, viewModel, object : ApiListener {
            override fun onSuccess() {
            }
            override fun onFail(errMsg: String?) {
            }
        })
    }

    private fun subscribesShadowTopic(mqttManager: AWSIotMqttManager?, viewModel: MqttViewModel?,
                                      thingName: String?) {
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/get/accepted", viewModel)
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/get/rejected", viewModel)
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/update/delta", viewModel)
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/update/accepted", viewModel)
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/update/documents", viewModel)
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/update/rejected", viewModel)
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/accepted", viewModel)
        subscrb2Topic(thingName, "\$aws/things/${thingName}/shadow/rejected", viewModel)

    }


    fun getKeyStore(viewModel: MqttViewModel?): KeyStore? {
        var keystore: KeyStore? = null

        if (!viewModel?.mSavedCertId.isNullOrEmpty() && !viewModel?.mSavedCertPem.isNullOrEmpty() &&
        !viewModel?.mSavedPrivateKey.isNullOrEmpty()) {
            try {
                AWSIotKeystoreHelper.saveCertificateAndPrivateKey(
                    viewModel?.mSavedCertId, viewModel?.mSavedCertPem,
                    viewModel?.mSavedPrivateKey, viewModel?.mKeyStorePath!!,
                    viewModel?.mKeystoreName, viewModel?.mKeyStorePw
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            keystore = Util.getCertKeyStore(viewModel?.mKeyStorePath!!,
                viewModel?.mSavedCertId!!, viewModel?.mKeystoreName!!, viewModel?.mKeyStorePw!!)

        } else {
            keystore = Util.getCertKeyStore(
                viewModel?.mKeyStorePath!!,
                SharedPreferenceManager.getPreference(ctx,
                    SharedPreferenceManager.CERT_ID, ""),
                SharedPreferenceManager.getPreference(ctx,
                    SharedPreferenceManager.KEYSTORE_NAME, ""),
                SharedPreferenceManager.getPreference(ctx,
                    SharedPreferenceManager.KEYSTORE_PW, "")
            )

            if (!SharedPreferenceManager.getPreference(ctx,
                    SharedPreferenceManager.IS_SUCCESSED_PRIVISIONING, false)) {
                keystore = null
            }
        }
        return keystore
    }

}