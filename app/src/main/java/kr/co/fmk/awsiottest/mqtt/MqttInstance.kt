package kr.co.fmk.awsiottest.mqtt

import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager
import com.amazonaws.regions.Region

object MqttInstance {
    private var mqttManager: AWSIotMqttManager? = null
    @JvmStatic
    fun getMqttInstance(thingName : String?, region : Region?, accountEndpointPrefix : String?) : AWSIotMqttManager? {
        if (mqttManager == null) {
            mqttManager = AWSIotMqttManager(thingName, region, accountEndpointPrefix)
        }
        return mqttManager
    }
}