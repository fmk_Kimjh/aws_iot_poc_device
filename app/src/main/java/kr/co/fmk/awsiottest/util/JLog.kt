package kr.co.fmk.awsiottest.util

import android.util.Log

object JLog {
    var hasLog = true

    @JvmStatic
    fun d(tag: String?, func: String, msg: String) {
        if (hasLog) {
            Log.d(tag, "$func::$msg")
        }
    }

    fun e(tag: String?, func: String, msg: String) {
        if (hasLog) {
            Log.e(tag, "$func::$msg")
        }
    }

    fun i(tag: String?, func: String, msg: String) {
        if (hasLog) {
            Log.i(tag, "$func::$msg")
        }
    }

    @JvmStatic
    fun w(tag: String?, func: String, msg: String) {
        if (hasLog) {
            Log.w(tag, "$func::$msg")
        }
    }

    fun v(tag: String?, func: String, msg: String) {
        if (hasLog) {
            Log.v(tag, "$func::$msg")
        }
    }
}