package kr.co.fmk.awsiottest.vo

class RegisterThingRequestVo {
    var certificateOwnershipToken: String? = null
    var templateName: String? = null
    var parameters: HashMap<String, String>? = null
    override fun toString(): String {
        return """
            RegisterThingRequestVo(
            certificateOwnershipToken=$certificateOwnershipToken
            templateName=$templateName
            parameters=${parameters.toString()}
            )
            """
    }
}