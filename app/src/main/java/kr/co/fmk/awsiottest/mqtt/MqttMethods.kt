package kr.co.fmk.awsiottest.mqtt

import android.content.Context
import android.util.Log
import com.amazonaws.mobileconnectors.iot.*
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import kr.co.fmk.awsiottest.Common
import kr.co.fmk.awsiottest.viewmodels.MqttViewModel
import com.amazonaws.mobileconnectors.iot.AWSIotMqttSubscriptionStatusCallback as AWSIotMqttSubscriptionStatusCallback

class MqttMethods(private val ctx: Context) {
    private val TAG = javaClass.simpleName.toString()

    fun publish2Topic(thingName: String?, topic: String?, msg: String?,
                      viewModel: MqttViewModel?, apiListener: ApiListener?) {
        val mqttManager = MqttInstance.getMqttInstance(thingName,
            Region.getRegion(Regions.AP_NORTHEAST_2),
            Common.ACCOUNT_ENDPOINT_PREFIX)

        var userData: Object? = null

        mqttManager?.publishString(msg, topic, AWSIotMqttQos.QOS0,
            AWSIotMqttMessageDeliveryCallback { status, throwable ->
                if (status == AWSIotMqttMessageDeliveryCallback.MessageDeliveryStatus.Success) {
                    apiListener?.onSuccess()
                } else {
                    apiListener?.onFail(throwable?.toString())
                }
            }, userData)
        Log.d(TAG, userData.toString())
    }

    fun subsribe2Topic(thingName: String?, topic: String?,
                      viewModel: MqttViewModel?, apiListener: ApiListener?) {
        val mqttManager = MqttInstance.getMqttInstance(thingName,
            Region.getRegion(Regions.AP_NORTHEAST_2),
            Common.ACCOUNT_ENDPOINT_PREFIX)

        mqttManager?.subscribeToTopic(topic, AWSIotMqttQos.QOS0,
            object: AWSIotMqttSubscriptionStatusCallback {
                override fun onSuccess() {
                    apiListener?.onSuccess()
                }
                override fun onFailure(exception: Throwable) {
                    apiListener?.onFail(exception?.message)
                }
            },
            object: AWSIotMqttNewMessageCallback {
                override fun onMessageArrived(topic: String?, data: ByteArray?) {
                    viewModel?.setSubResult("topic : $topic, data : ${data?.let { String(it) }}")

                }
            })

    }

}