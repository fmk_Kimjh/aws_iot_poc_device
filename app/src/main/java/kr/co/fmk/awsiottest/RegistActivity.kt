package kr.co.fmk.awsiottest

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.amazonaws.mobileconnectors.iot.AWSIotKeystoreHelper
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.google.gson.Gson
import kr.co.fmk.awsiottest.databinding.ActivityRegistBinding
import kr.co.fmk.awsiottest.util.JLog
import kr.co.fmk.awsiottest.util.SharedPreferenceManager
import kr.co.fmk.awsiottest.util.Util
import kr.co.fmk.awsiottest.vo.CreateKeyCertificateResponseVo
import kr.co.fmk.awsiottest.vo.ErrorResponseVo
import kr.co.fmk.awsiottest.vo.RegisterThingRequestVo
import kr.co.fmk.awsiottest.vo.RegisterThingResponseVo
import java.security.KeyStore

class RegistActivity : AppCompatActivity() {
    private val TAG = javaClass.simpleName.toString()
    private val binding by lazy { ActivityRegistBinding.inflate(layoutInflater) }
    private var deviceSerialNumber: String? = null

    var createKeysAndCertificateResponse: CreateKeyCertificateResponseVo? = null
    var registerThingResponse: RegisterThingResponseVo? = null

    var mqttManager: AWSIotMqttManager? = null

    private val templateName = "xxxx"
    private val CERT_ID = "xxxx"
    private var KEY_STORE_NAME = "xxxx.pks"
    private var KEY_STORE_PASSWORD = "xxxx"
    private val CERT_FILE =
        "xxxx-certificate.pem.crt"
    private val PRIVATE_KEY_FILE =
        "xxxx-private.pem.key"

    private var keyStorePath: String? = null
    private var keyStore: KeyStore? = null

    companion object {
        var savedCertId: String? = null
        var savedCertPem: String? = null
        var savedPrivateKey: String? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
    }

    @SuppressLint("HardwareIds")
    private fun initViews() {
        deviceSerialNumber = Build.SERIAL
        mqttManager = AWSIotMqttManager(
            "device_$deviceSerialNumber",
            Region.getRegion(Regions.AP_NORTHEAST_2),
            Common.ACCOUNT_ENDPOINT_PREFIX
        )
        binding.btnConnect.setOnClickListener {

            keyStorePath = filesDir.path

            Log.d(TAG, "CERT_ID : $CERT_ID")
            Log.d(TAG, "keyStorePath : $keyStorePath")
            Log.d(TAG, "KEY_STORE_NAME : $KEY_STORE_NAME")
            Log.d(TAG, "KEY_STORE_PASSWORD : $KEY_STORE_PASSWORD")

            saveCertificateAndPrivateKey(keyStorePath)
            val isContain = AWSIotKeystoreHelper.keystoreContainsAlias(
                CERT_ID,
                keyStorePath,
                KEY_STORE_NAME,
                KEY_STORE_PASSWORD
            )
            if (isContain) {
                keyStore = AWSIotKeystoreHelper.getIotKeystore(
                    CERT_ID,
                    keyStorePath,
                    KEY_STORE_NAME,
                    KEY_STORE_PASSWORD
                )
            }
            Log.d(TAG, "isContain : $isContain")
            mqttManager?.connect(
                keyStore
            ) { status, throwable ->
                Log.d(
                    TAG, "status : $status, throwable : ${throwable?.message}"
                )
                runOnUiThread {
                    binding.tvInform.text = "status : $status, throwable : ${throwable?.message}, deviceSerialNumber : $deviceSerialNumber"
                }
                if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connected) {
                    mqttManager?.subscribeToTopic(
                        "\$aws/certificates/create/json/accepted",
                        AWSIotMqttQos.QOS1
                    ) { topic, data ->
                        JLog.d(TAG, Util.methodName, "topic : $topic, data : ${String(data)}")
                        onCreateKeysAndCertificateAccepted(
                            Gson().fromJson(
                                String(data),
                                CreateKeyCertificateResponseVo::class.java
                            )
                        )
                    }
                    mqttManager?.subscribeToTopic(
                        "\$aws/certificates/create/json/rejected",
                        AWSIotMqttQos.QOS1
                    ) { topic, data ->
                        JLog.d(TAG, Util.methodName, "topic : $topic, data : ${String(data)}")
                        onRejectedKeys(
                            Gson().fromJson(
                                String(data),
                                ErrorResponseVo::class.java
                            )
                        )
                    }
                    mqttManager?.subscribeToTopic(
                        "\$aws/provisioning-templates/$templateName/provision/json/accepted",
                        AWSIotMqttQos.QOS1
                    ) { topic, data ->
                        JLog.d(TAG, Util.methodName, "topic : $topic, data : ${String(data)}")
                        onRegisterThingAccepted(
                            Gson().fromJson(
                                String(data),
                                RegisterThingResponseVo::class.java
                            )
                        )
                    }
                    mqttManager?.subscribeToTopic(
                        "\$aws/provisioning-templates/$templateName/provision/json/rejected",
                        AWSIotMqttQos.QOS1
                    ) { topic, data ->
                        JLog.d(TAG, Util.methodName, "topic : $topic, data : ${String(data)}")
                        onRejectedRegister(
                            Gson().fromJson(
                                String(data),
                                ErrorResponseVo::class.java
                            )
                        )
                    }
                }
            }
        }
        binding.btnDisconnect.setOnClickListener {
        }
        binding.btnSub.setOnClickListener {
        }
        binding.btnUnsub.setOnClickListener {
        }
        binding.btnPublishCreateCert.setOnClickListener {
            mqttManager?.publishString("", "\$aws/certificates/create/json", AWSIotMqttQos.QOS1)
        }
        binding.btnPublishRegist.setOnClickListener {
            val request = RegisterThingRequestVo()
            request.certificateOwnershipToken =
                createKeysAndCertificateResponse?.certificateOwnershipToken
            request.templateName = templateName
            request.parameters = HashMap<String, String>().apply {
                put("SerialNumber", deviceSerialNumber ?: "unknown")
                put("Password", "fmk1234!")
                put("DeviceLocation", "Seoul")
                put(
                    "DisPlayOrientation",
                    when (Build.MODEL) {
                        "FMKEV25-2" -> "Portrait"
                        "FMKEV27-1" -> "Landscape"
                        else -> "Portrait"
                    }
                )
            }
            JLog.d(TAG, Util.methodName, request.parameters.toString())
            val payloadJson: String = Gson().toJson(request)
            mqttManager?.publishString(
                payloadJson,
                "\$aws/provisioning-templates/$templateName/provision/json",
                AWSIotMqttQos.QOS1
            )
        }
    }

    private fun saveCertificateAndPrivateKey(keyStorePath: String?) {
        val cert: String? = Util.assetsFileToString(this, CERT_FILE)
        val priv: String? = Util.assetsFileToString(this, PRIVATE_KEY_FILE)
        try {
            AWSIotKeystoreHelper.saveCertificateAndPrivateKey(
                CERT_ID,
                cert,
                priv,
                keyStorePath,
                KEY_STORE_NAME,
                KEY_STORE_PASSWORD
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun onRejectedKeys(response: ErrorResponseVo?) {
        JLog.d(TAG, Util.methodName, response.toString())
        runOnUiThread {
            binding.tvResult.text = "${Util.methodName}, response : ${response.toString()}}"
        }
    }

    private fun onRejectedRegister(response: ErrorResponseVo?) {
        JLog.d(TAG, Util.methodName, response.toString())
        runOnUiThread {
            binding.tvResult.text = "${Util.methodName}, response : ${response.toString()}}"
        }
    }

    private fun onCreateKeysAndCertificateAccepted(response: CreateKeyCertificateResponseVo?) {
        if (response != null) {
            JLog.d(TAG, Util.methodName, response.toString())
            createKeysAndCertificateResponse = response
            runOnUiThread {
                binding.tvInform.text = "${Util.methodName}, response : ${response.toString()}}"
            }
        } else {
            JLog.d(TAG, Util.methodName, "CreateKeysAndCertificate response is null")
        }
    }

    private fun onRegisterThingAccepted(response: RegisterThingResponseVo?) {
        if (response != null) {
            JLog.d(TAG, Util.methodName, response.toString())
            registerThingResponse = response
            runOnUiThread {
                binding.tvInform.text = "${Util.methodName}, response : ${response.toString()}}"
            }
            SharedPreferenceManager.setPreference(this,
                SharedPreferenceManager.THING_NAME, registerThingResponse?.thingName)
            savedCertId = createKeysAndCertificateResponse?.certificateId
            savedCertPem = createKeysAndCertificateResponse?.certificatePem
            savedPrivateKey = createKeysAndCertificateResponse?.privateKey
            startActivity(Intent(this, MqttAndActivity::class.java))
        } else {
            JLog.d(TAG, Util.methodName, "RegisterThing response is null")
        }
    }

    /*fun waitForKeysRequest() {
        try {
            // Wait for the response.
            var loopCount = 0
            while (loopCount < 30 && createKeysAndCertificateResponse == null) {
                if (createKeysAndCertificateResponse != null) {
                    break
                }
                JLog.d(TAG, Util.methodName, "Waiting...for CreateKeysAndCertificateResponse")
                loopCount += 1
                Thread.sleep(50L)
            }
        } catch (e: InterruptedException) {
            JLog.d(TAG, Util.methodName, "Exception occured")
        }
    }

    fun waitForRegisterRequest() {
        try {
            // Wait for the response.
            var loopCount = 0
            while (loopCount < 30 && registerThingResponse == null) {
                if (registerThingResponse != null) {
                    break
                }
                JLog.d(TAG, Util.methodName, "Waiting...for registerThingResponse")
                loopCount += 1
                Thread.sleep(50L)
            }
        } catch (e: InterruptedException) {
            JLog.d(TAG, Util.methodName, "Exception occured")
        }
    }*/

    override fun onDestroy() {
        super.onDestroy()
        mqttManager?.disconnect()
    }
}