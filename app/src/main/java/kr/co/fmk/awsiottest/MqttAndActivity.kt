package kr.co.fmk.awsiottest

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager
import kr.co.fmk.awsiottest.databinding.ActivityMqttBinding
import kr.co.fmk.awsiottest.mqtt.ApiListener
import kr.co.fmk.awsiottest.mqtt.MqttConnection
import kr.co.fmk.awsiottest.mqtt.MqttMethods
import kr.co.fmk.awsiottest.util.JLog
import kr.co.fmk.awsiottest.util.SharedPreferenceManager
import kr.co.fmk.awsiottest.util.Util
import kr.co.fmk.awsiottest.viewmodels.MqttViewModel
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class MqttAndActivity : AppCompatActivity() {
    private val TAG = javaClass.simpleName.toString()
    private val binding by lazy { ActivityMqttBinding.inflate(layoutInflater) }
    private var mqttManager: AWSIotMqttManager? = null
    private var viewModel: MqttViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        viewModel = MqttViewModel(this@MqttAndActivity)
        binding.setLifecycleOwner(this@MqttAndActivity)
        binding.viewModel = viewModel

        JLog.d(TAG, Util.methodName, Build.SERIAL)
        checkProvisioning()
    }

    private fun checkProvisioning() {

        viewModel?.mSavedCertId = RegistActivity.savedCertId
        viewModel?.mSavedCertPem = RegistActivity.savedCertPem
        viewModel?.mSavedPrivateKey = RegistActivity.savedPrivateKey
        viewModel?.mKeystoreName = "previsioning_cert_${System.currentTimeMillis()}.jks"
        viewModel?.mKeyStorePath = filesDir.path

        init()
    }

    private fun init() {
        val thingName = SharedPreferenceManager.getPreference(this,
            SharedPreferenceManager.THING_NAME, "")

        binding.btnConnect.setOnClickListener {
            val mqttConnection = MqttConnection(this@MqttAndActivity)
            var keystore = mqttConnection.getKeyStore(viewModel)
            if (keystore != null) {
                viewModel?.setSubResult("provision 완료됨.\nThingName : $thingName")

                mqttConnection.connect(thingName, keystore, viewModel, object : ApiListener {
                    override fun onSuccess() {}
                    override fun onFail(errMsg: String?) {}
                })
            } else {
                startActivity(Intent(this, RegistActivity::class.java))
                finish()
            }
        }

        binding.btnPublish.setOnClickListener {
            val topic = "fmk_topic1"
            val msg = binding.etInput.text.toString()

            if (!thingName.isNullOrEmpty() && !msg.isNullOrEmpty()) {
                val mqttMethods = MqttMethods(this@MqttAndActivity)
                mqttMethods.publish2Topic(thingName, topic, msg, viewModel, object : ApiListener {
                    override fun onSuccess() {
                    }

                    override fun onFail(errMsg: String?) {
                    }
                })
            }
        }

        binding.btnSubscribe.setOnClickListener {
            val topic = "fmk_topic2"

            if (!thingName.isNullOrEmpty()) {
                val mqttMethods = MqttMethods(this@MqttAndActivity)
                mqttMethods.subsribe2Topic(thingName, topic, viewModel, object : ApiListener {
                    override fun onSuccess() {
                        viewModel?.setSubResult("topic : $topic connected")
                    }

                    override fun onFail(errMsg: String?) {
                        viewModel?.setSubResult("topic : $topic error: $errMsg")
                    }
                })
            }
        }

        binding.btnDeviceShadowGet.setOnClickListener {
            val topic = "\$aws/things/${thingName}/shadow/get"
            val msg = ""
            viewModel?.setResponseResult("")

            if (!thingName.isNullOrEmpty()) {
                val mqttMethods = MqttMethods(this@MqttAndActivity)
                mqttMethods.publish2Topic(thingName, topic, msg, viewModel, object : ApiListener {
                    override fun onSuccess() {
                    }

                    override fun onFail(errMsg: String?) {
                    }
                })
            }
        }

        binding.btnDeviceShadowDelete.setOnClickListener {
            val topic = "\$aws/things/${thingName}/shadow/delete"
            val msg = ""
            viewModel?.setResponseResult("")

            if (!thingName.isNullOrEmpty()) {
                val mqttMethods = MqttMethods(this@MqttAndActivity)
                mqttMethods.publish2Topic(thingName, topic, msg, viewModel, object : ApiListener {
                    override fun onSuccess() {
                    }

                    override fun onFail(errMsg: String?) {
                    }
                })
            }
        }

        binding.btnDeviceShadowDesireUpdate.setOnClickListener {
            val reqObj = JSONObject().apply {
                put("address", binding.etInputAddress.text.toString())
                put("volume", binding.etInputVolume.text.toString().toInt())
                put("backlight", binding.etInputBacklight.text.toString())
            }
            val jsonObj = JSONObject().apply {
                put("desired", reqObj)
            }
            val reqJsonStr = JSONObject().apply {
                put("state", jsonObj)
            }
            val msg = reqJsonStr.toString()
            JLog.d(TAG, Util.methodName, "msg : $msg")
            val topic = "\$aws/things/${thingName}/shadow/update"
            viewModel?.setResponseResult("")

            if (!thingName.isNullOrEmpty() && !msg.isNullOrEmpty()) {
                val mqttMethods = MqttMethods(this@MqttAndActivity)
                mqttMethods.publish2Topic(thingName, topic, msg, viewModel, object : ApiListener {
                    override fun onSuccess() {
                    }

                    override fun onFail(errMsg: String?) {
                    }
                })
            }
        }
        binding.btnDeviceShadowReportUpdate.setOnClickListener {
            val reqObj = JSONObject().apply {
                put("address", binding.etInputAddress.text.toString())
                put("volume", binding.etInputVolume.text.toString().toInt())
                put("backlight", binding.etInputBacklight.text.toString())
            }
            val jsonObj = JSONObject().apply {
                put("reported", reqObj)
            }
            val reqJsonStr = JSONObject().apply {
                put("state", jsonObj)
            }
            val msg = reqJsonStr.toString()
            JLog.d(TAG, Util.methodName, "msg : $msg")
            val topic = "\$aws/things/${thingName}/shadow/update"
            viewModel?.setResponseResult("")

            if (!thingName.isNullOrEmpty() && !msg.isNullOrEmpty()) {
                val mqttMethods = MqttMethods(this@MqttAndActivity)
                mqttMethods.publish2Topic(thingName, topic, msg, viewModel, object : ApiListener {
                    override fun onSuccess() {
                    }

                    override fun onFail(errMsg: String?) {
                    }
                })
            }
        }

        binding.btnSendLong.setOnClickListener {
            val topic = "fmk/it/${thingName}/topic/testing"
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

            val reqObj = JSONObject().apply {
                put("date", formatter.format(System.currentTimeMillis()))
                put("volume", binding.etInputVolume.text.toString().toInt())
                put("address", binding.etInputAddress.text.toString())
            }
            val msg = reqObj.toString()
            viewModel?.setResponseResult("")
            JLog.d(TAG, Util.methodName, msg)
            if (!thingName.isNullOrEmpty()) {
                val mqttMethods = MqttMethods(this@MqttAndActivity)
                mqttMethods.publish2Topic(thingName, topic, msg, viewModel, object : ApiListener {
                    override fun onSuccess() {
                        viewModel?.setResponseResult("${Util.methodName}, msg : $msg")
                    }

                    override fun onFail(errMsg: String?) {
                        viewModel?.setResponseResult("${Util.methodName}, msg : $msg")
                    }
                })
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        mqttManager?.disconnect();
    }
}