package kr.co.fmk.awsiottest.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData

class MqttViewModel(private val ctx: Context) {
    @JvmField
    var pubResult = MutableLiveData<String>()
    @JvmField
    var subResult = MutableLiveData<String>()
    @JvmField
    var responseResult = MutableLiveData<String>()
    @JvmField
    var inputAddress = MutableLiveData<String>()
    @JvmField
    var volume = MutableLiveData<String>()
    @JvmField
    var backlight = MutableLiveData<String>()

    var mSavedCertId: String? = null
    var mSavedCertPem: String? = null
    var mSavedPrivateKey: String? = null
    var mKeystoreName: String? = null
    val mKeyStorePw: String? = "abcd!"
    var mKeyStorePath: String? = null

    init {
        pubResult.postValue("")
        subResult.postValue("")
        responseResult.postValue("")
        inputAddress.postValue("서울시 강남구 개포동")
        volume.postValue("5")
        backlight.postValue("on")
    }

    fun setPubResult(dt: String?) {
        pubResult.postValue(dt)
    }
    fun setSubResult(dt: String?) {
        subResult.postValue(dt)
    }
    fun setResponseResult(dt: String?) {
        responseResult.postValue(dt)
    }


}