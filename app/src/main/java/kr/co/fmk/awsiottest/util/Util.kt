package kr.co.fmk.awsiottest.util

import android.app.ActivityManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.SystemClock
import android.util.Log
import android.util.TypedValue
import com.amazonaws.mobileconnectors.iot.AWSIotKeystoreHelper
import java.security.KeyStore
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*

object Util {
    private val TAG = Util::class.java.simpleName.trim { it <= ' ' }

    @JvmStatic
    val methodName: String
        get() {
            var elements: Array<StackTraceElement>? = null
            elements = try {
                throw Exception("getMethodName")
            } catch (e: Exception) {
                e.stackTrace
            }
            return elements!![1].methodName
        }

    fun getToday(pattern: String?): String {
        val nation = Locale("ko", "KOREA")
        val date = Date()
        val cal = Calendar.getInstance()
        cal.isLenient = true
        cal.time = date
        return SimpleDateFormat(pattern, nation).format(date)
    }

    fun getAppVersion(context: Context, packageName: String?): Array<String>? {
        return try {
            val pinfo =
                packageName?.let { context.packageManager.getPackageInfo(it, 0) } ?: return null
            arrayOf(pinfo.versionName, pinfo.versionCode.toString())
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            null
        }
    }


    fun sha256ToString(str: String, key: String): String? {
        val input = str + key
        var result: String? = null
        try {
            val md = MessageDigest.getInstance("SHA-256")
            md.update(input.toByteArray())
            val bytes = md.digest()
            val sb = StringBuffer()
            for (b in bytes) {
                sb.append(String.format("%02x", b))
            }
            result = sb.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return result
    }

    fun getDpToPixel(context: Context, DP: Float): Int {
        var px = 0f
        try {
            px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, DP,
                context.resources.displayMetrics
            )
        } catch (e: Exception) {
        }
        return px.toInt()
    }

    fun secToMinSec(sec: Int): String {
        return (sec % 3600 / 60).toString() + ":" + String.format("%02d", sec % 3600 % 60)
    }

    val curTime: Long
        get() = SystemClock.uptimeMillis()

    fun isLaunchingService(context: Context, clsName: String): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (clsName == service.service.className) {
                return true
            }
        }
        return false
    }

    fun assetsFileToString(context: Context, file: String): String? {
        val str = context.assets.open(file).bufferedReader().use { it.readText() }
        Log.d(TAG, "assetsFileToString : $str")
        return str
    }

    fun getCertKeyStore(path: String, certId: String, name: String, password: String): KeyStore? {
        if (path.isNotEmpty() && certId.isNotEmpty() && name.isNotEmpty() && password.isNotEmpty()) {
            val isPresent = AWSIotKeystoreHelper.isKeystorePresent(path, name)
            if (isPresent) {
                val isContain = AWSIotKeystoreHelper.keystoreContainsAlias(
                    certId,
                    path,
                    name,
                    password
                )
                if (isContain) {
                    return AWSIotKeystoreHelper.getIotKeystore(
                        certId,
                        path,
                        name,
                        password
                    )
                }
            }
        }
        return null
    }
}