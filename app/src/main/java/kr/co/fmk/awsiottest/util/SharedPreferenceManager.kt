package kr.co.fmk.awsiottest.util

import android.content.Context

object SharedPreferenceManager {
    const val IS_SUCCESSED_PRIVISIONING = "is_successed_privisioning"
    const val CERT_ID = "cert_id"
    const val KEYSTORE_NAME = "keystore_name"
    const val KEYSTORE_PW = "keystore_pw"
    const val THING_NAME = "thing_name"

    fun setPreference(context: Context, key: String?, value: String?) {
        val sharedPreferences = context.getSharedPreferences("pref_wbm", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString(key, value)
        edit.apply()
    }

    fun setPreference(context: Context, key: String?, value: Boolean) {
        val sharedPreferences = context.getSharedPreferences("pref_wbm", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putBoolean(key, value)
        edit.commit()
    }

    fun setPreference(context: Context, key: String?, value: Int) {
        val sharedPreferences = context.getSharedPreferences("pref_wbm", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putInt(key, value)
        edit.commit()
    }

    fun getPreference(context: Context, key: String?, defValue: String): String {
        val sharedPreferences = context.getSharedPreferences("pref_wbm", Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, defValue) ?: ""
    }

    fun getPreference(context: Context, key: String?, defValue: Boolean): Boolean {
        val sharedPreferences = context.getSharedPreferences("pref_wbm", Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(key, defValue)
    }

    fun getPreference(context: Context, key: String?, defValue: Int): Int {
        val sharedPreferences = context.getSharedPreferences("pref_wbm", Context.MODE_PRIVATE)
        return sharedPreferences.getInt(key, defValue)
    }
}